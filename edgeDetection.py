#Implements edge detection on an image using Sobel Operator
from PIL import Image, ImageFilter, ImageOps
import sys
def edgeDetection(imageName):
	#open image and conert to grayscale
	im = Image.open(imageName)
	im = ImageOps.grayscale(im)
	im2 = im.copy()
	#Dimensions
	size = im.getbbox()
	columns = size[2]
	rows = size[3]

	for x in range(0,columns):
		for y in range(0,rows):
			p = build3x3(x,y,im,rows,columns)
			pixel = sobel(p)
			im2.putpixel((x,y),pixel)


	#print('{}{}{}'.format(width,'x',height))

	im2.show()
	im2.save('transformed.png')

#Returns array of 3x3 block where center entry is input pixel
def build3x3(x,y,im,rows,columns):
	#Creates 1d array of all pixels by iterating through rows
	pixels = im.getdata()

	upperleft, above, upperright, left, center, right, lowerleft, below, lowerright = 0,0,0,0,0,0,0,0,0

	#Uses row major order to take neighboring pixels out of 1d array
	if x > 0:
		if y > 0:
			upperleft = pixels[((y-1)*columns)+(x-1)]
			above = pixels[((y-1)*columns)+x]
		left = pixels[(y*columns)+(x-1)]
		center = pixels[(y*columns)+x]
		if y < rows-1:
			lowerleft = pixels[((y+1)*columns)+(x-1)]
			below = pixels[((y+1)*columns)+x]

	if x < columns-1:
		if y > 0:
			upperright = pixels[((y-1)*columns)+(x+1)]
		right = pixels[(y*columns)+(x+1)]
		if  y < rows-1:
			lowerright = pixels[((y+1)*columns)+(x+1)]

	block = (upperleft,above,upperright,left,center,right,lowerleft,below,lowerright)
	#print(block)
	return block




#Implementation of Sobel Operator
def sobel(block):
	x = block[0*3+0] + 2*block[1*3+0] + block[2*3+0]
	x -= block[0*3+2] + 2*block[1*3+2] + block[2*3+2]
	y = block[0*3+0] + 2*block[0*3+1] + block[0*3+2]
	y -= block[2*3+0] + 2*block[2*3+1] + block[2*3+2]
	r = (x*x+y*y)**(.5)
	#print(r)
	if(r > 255):
		r = 255
	return r

if __name__ == '__main__':
    edgeDetection(sys.argv[1])


	